init:
	docker-compose -f docker-compose-e2e.yml build

serve:
	docker-compose up

test:
	docker-compose -f docker-compose-e2e.yml up --abort-on-container-exit
