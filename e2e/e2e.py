import json
import os

from requests import get, post

CLEANSE_URL = f'http://api:5000/cleanse'

def test_post_failing_id():
    expected = {
        'error': '504 Gateway Timeout',
        'message': 'The connection to an upstream server timed out.'
    }

    user = {
        'id': '0003'
    }

    response = post(CLEANSE_URL, json=user)

    actual = response.json()

    assert expected == actual, f'EXPECTED: {expected} != ACTUAL: {actual}'


def test_post_obfuscating_id():
    expected = {
        'favouriteColour': 'red',
        'id': '74327ebb14',
        'rowId': 1
    }

    user = {
        'favouriteColour': 'red',
        'rowId': 1,
        'id': '0001'
    }

    response = post(CLEANSE_URL, json=user)

    actual = response.json()

    assert expected == actual, f'EXPECTED: {expected} != ACTUAL: {actual}'


def test_post_non_obfuscating_id():
    expected = {
        'favouriteColour': 'green',
        'id': '0002',
        'rowId': 2
    }

    user = {
        'favouriteColour': 'green',
        'rowId': 2,
        'id': '0002'
    }

    response = post(CLEANSE_URL, json=user)

    actual = response.json()

    assert expected == actual, f'EXPECTED: {expected} != ACTUAL: {actual}'
