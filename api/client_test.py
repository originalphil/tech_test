import json
import time
import unittest
from unittest.mock import Mock, patch, PropertyMock

from config import RETRIES
from requests.exceptions import HTTPError

from client import Client

class TestClient(unittest.TestCase):
    @patch('client.requests')
    def test_client_no_exceptions(self, mock_requests):
        mock_response = Mock()
        json_string = json.dumps({'newId': 'f432e601'})
        type(mock_response).text = PropertyMock(return_value=json_string)

        mock_requests.get.return_value = mock_response

        client = Client(Mock())
        result = client.get_id('0000')

        assert result == 'f432e601'


    @patch('client.requests')
    def test_client_retry(self, mock_requests):
        mock_response = Mock()
        mock_response.raise_for_status.side_effect = HTTPError

        mock_requests.get.return_value = mock_response

        client = Client(Mock())

        with self.assertRaises(HTTPError):
            result = client.get_id('0000')

        assert mock_requests.get.call_count == RETRIES
