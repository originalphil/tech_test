import json
import time

import config
from requests.models import Request

from responses import TIMEOUT_RESPONSE
from validator import Validator
from client import Client

class Cleaner:
    def __init__(self, validator: Validator, client: Client, logger):
        self.logger = logger
        self.validator = validator
        self.client = client

    def replace_id(self, request: Request) -> dict:
        self.logger.info('Request received for cleansing.')
        valid, error_response = self.validator.validate(request)
        if not valid:
            return error_response

        response = request.get_json()
        identifier = response['id']

        try:
            self.logger.info(f'Request valid. Proceeding with cleansing for id: {identifier}')
            cleaned_id = self.client.get_id(identifier)
        except:
            self.logger.error(f'PreferenceProvider server timed out.')
            return TIMEOUT_RESPONSE

        if not cleaned_id:
            self.logger.info(f'Id: {identifier} not obfuscated due to preference.')
            return response

        response['id'] = cleaned_id
        self.logger.info(f'Replaced id: {identifier} with new_id: {cleaned_id}')

        return response
