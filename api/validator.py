from requests.models import Request

from responses import INVALID_ID_RESPONSE, INVALID_JSON_RESPONSE, NO_ID_RESPONSE

class Validator:
    def __init__(self, logger):
        self.logger = logger

    def validate(self, request: Request) -> (bool, dict):
        self.logger.info('Request received for validation.')

        validations = [
            self._validate_json,
            self._validate_id_present,
            self._validate_id_type,
            self._validate_id_length,
            self._validate_id_parse,
            self._validate_id_range,
        ]

        for v in validations:
            valid, error_response = v(request)
            if not valid:
                return False, error_response

        response = request.get_json()

        return True, response


    def _validate_json(self, request: Request) -> (bool, dict):
        try:
            request_json = request.get_json()
        except:
            self.logger.info('Invalid json.')
            return False, INVALID_JSON_RESPONSE
        return True, None


    def _validate_id_present(self, request: Request) -> (bool, dict):
        request_json = request.get_json()
        try:
            identifier = request_json['id']
        except:
            self.logger.info('No id.')
            return False, NO_ID_RESPONSE
        return True, None


    def _validate_id_type(self, request: Request) -> (bool, dict):
        identifier = request.get_json()['id']
        if not isinstance(identifier, str):
            return False, INVALID_ID_RESPONSE
        return True, None


    def _validate_id_length(self, request: Request) -> (bool, dict):
        identifier = request.get_json()['id']
        if len(identifier) != 4:
            self.logger.info('Id is not four chars.')
            return False, INVALID_ID_RESPONSE
        return True, None


    def _validate_id_parse(self, request: Request) -> (bool, dict):
        identifier = request.get_json()['id']
        try:
            int(identifier)
        except:
            self.logger.info('Id could not be parsed.')
            return False, INVALID_ID_RESPONSE
        return True, None


    def _validate_id_range(self, request: Request) -> (bool, dict):
        identifier = int(request.get_json()['id'])
        if identifier < 0 or identifier > 9999:
            self.logger.info('Id out of range.')
            return False, INVALID_ID_RESPONSE
        return True, None
