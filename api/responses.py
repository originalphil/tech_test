INVALID_JSON_RESPONSE = {
    'error': 'INVALID_JSON',
    'message': 'The request was not in a valid JSON format.'
}

INVALID_ID_RESPONSE = {
    'error': 'INVALID_ID',
    'message': 'The provided ID is invalid.'
}

NO_ID_RESPONSE = {
    'error': 'NO_ID',
    'message': 'The request does not contain an ID.'
}

TIMEOUT_RESPONSE = {
    'error': '504 Gateway Timeout',
    'message': 'The connection to an upstream server timed out.'
}
