import json
import logging
from logging import StreamHandler
import os
import sys

from flask import abort, Flask, jsonify, request

from cleaner import Cleaner
from client import Client
from validator import Validator

app = Flask(__name__)

env = os.environ.get('ENV', '')

@app.route('/id/<identifier>')
def id(identifier: str):
    client = Client(app.logger)
    new_id = client.get_id(identifier)

    return jsonify({'id': new_id})


@app.route('/cleanse', methods=['POST'])
def cleanse():
    app.logger.info('POST request received at /cleanse')

    validator = Validator(app.logger)
    client = Client(app.logger)
    cleaner = Cleaner(validator, client, app.logger)

    response = cleaner.replace_id(request)
    app.logger.info(f'Returning response: {response}')

    return jsonify(response)


if __name__ == '__main__':
    handler = StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        "[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)

    app.logger.addHandler(handler)
    app.logger.setLevel(logging.DEBUG)

    log = logging.getLogger('werkzeug')
    log.setLevel(logging.DEBUG)
    log.addHandler(handler)

    app.run(host='0.0.0.0', debug=True)
