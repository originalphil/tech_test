import unittest
from unittest.mock import Mock

from validator import Validator

class ValidatorTest(unittest.TestCase):
    def test_api_client_validate_valid_identifiers(self):
        mock_logger = Mock()
        validator = Validator(mock_logger)

        valid_ids = [
            '0000',
            '5555',
            '9999',
        ]

        invalid_ids = []
        for identifier in valid_ids:
            mock_request = Mock()
            mock_request.get_json.return_value = {'id': identifier}
            valid, _ = validator.validate(mock_request)
            if not valid:
                invalid_ids.append(identifier)

        assert not invalid_ids, f'Invalid identifier(s): {invalid_ids}'


    def test_api_client_validate_invalid_identifiers(self):
        mock_logger = Mock()
        validator = Validator(mock_logger)

        invalid_ids = [
            '000',
            '-0001'
            'ABCD',
            '10000',
            1234,
            True
        ]

        valid_ids = []
        for identifier in invalid_ids:
            mock_request = Mock()
            mock_request.get_json.return_value = {'id': identifier}
            valid, _ = validator.validate(mock_request)
            if valid:
                valid_ids.append(identifier)

        assert not valid_ids, f'Valid identifier(s): {valid_ids}'
