import json
import time

import requests

from config import API_ROOT, RETRIES, RETRY_SECONDS

class Client:
    def __init__(self, logger):
        self.logger = logger

    def get_id(self, identifier: str, attempt: int=1) -> str:
        self.logger.info(f'Getting preference for id: {identifier}')
        try:
            response = requests.get(API_ROOT + identifier)
            response.raise_for_status()
        except:
            self.logger.warning(f'Exception raised on attempt {attempt} for id: {identifier}')
            if attempt < RETRIES:
                self.logger.info(f'Retrying for id: {identifier} after {RETRY_SECONDS}s')
                time.sleep(RETRY_SECONDS)
                self.get_id(identifier, attempt + 1)
            self.logger.error(f'Maximum attempts reached for id: {identifier}')
            raise

        new_id = json.loads(response.text)['newId']
        self.logger.info(f'id: {identifier} returned new_id: {new_id}')

        return new_id
